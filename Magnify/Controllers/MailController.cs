﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using RestSharp.Authenticators;

namespace Magnify.Controllers
{
    public class MailController : Controller
    {
        public static IRestResponse SendSimpleMessage(IdentityMessage message)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api",
                                "e23aad2422c3ea3aa7336e50a3ffd6ce-16ffd509-44017667"); RestRequest request = new RestRequest();
            request.AddParameter("domain", "xipecontactform.xipetechnology.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "<Info@xipetechnology.com>");
            request.AddParameter("to", message.Destination);
            //request.AddParameter("to", "will@xipetechnology.com");
            //request.AddParameter("to", "daniela@xipetechnology.com");
            //request.AddParameter("to", "oriol@xipetechnology.com");
            //request.AddParameter("to", "patty@xipetechnology.com");
            request.AddParameter("subject", message.Subject);
            request.AddParameter("html", message.Body);
            request.Method = Method.POST;
            var result = client.Execute(request);
            return result;
        }
    }
}
