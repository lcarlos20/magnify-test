﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Magnify.Models;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using System.Threading;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using System.Resources;
using System.Reflection;

namespace Magnify.Controllers
{
    public class HomeController : Controller
    {
        IConfiguration _configuration;
        private readonly IStringLocalizer<HomeController> _localizer;

        public static String ApiUrl = "http://thecontent.mx/api";

        public HomeController(IConfiguration configuration, IStringLocalizer<HomeController> localizer)
        {
            _configuration = configuration;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var apiUrl = ApiUrl;
            string mGuid = "676e30ab-286e-49c4-8723-114bd9238acd";
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            var idioma = CultureInfo.CurrentCulture;

            var categoryId = idioma.ToString() == "es" ? 1819 : 1820;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");


                }
            }
            catch (Exception)
            {

            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;


            var newsModel = JsonConvert.DeserializeObject<List<News>>(newsResponse).OrderByDescending(x => x.Date);

            var model = new IndexViewModel
            {
                News = newsModel.Where(x => x.CategoryId == categoryId).Take(2).Reverse().ToList(),
            };

            return View(model);
        }

        public IActionResult Como_iniciar()
        {
            return View();
        }

        public IActionResult Ventajas()
        {
            return View();
        }

        public IActionResult Precio()
        {
            return View();
        }

        public IActionResult FAQs()
        {
            return View();
        }

        [Route("/Home/Blog/{pagina?}")]
        public IActionResult Blog([FromRoute] int pagina = 0)

        {
            var apiUrl = ApiUrl;
            string mGuid = _configuration.GetValue<string>("mGuid");
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            var idioma = CultureInfo.CurrentCulture;
            var categoryId = idioma.ToString() == "es" ? 1819 : 1820;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");

                }
            }
            catch (Exception)
            {

            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            const int articulosPorPagina = 3;
            int SkipNews = pagina * articulosPorPagina;
            var newsModel = JsonConvert.DeserializeObject<List<News>>(newsResponse).OrderByDescending(x => x.Date);

            var model = new IndexViewModel
            {
                News = newsModel.Where(x => x.CategoryId == categoryId).Skip(SkipNews).Take(articulosPorPagina).Reverse().ToList(),
                PaginaActual = pagina,
                PaginasCount = newsModel.Count() / articulosPorPagina
            };

            return View(model);
        }

        public IActionResult Blog_Detalles(string id)
        {
            var apiUrl = "https://thecontent.mx/api";
            string mGuid = "676e30ab-286e-49c4-8723-114bd9238acd";
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");
                }
            }
            catch (Exception)
            {

            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;



            var noticias1 = JsonConvert.DeserializeObject<List<Magnify.Models.ArticleModel>>(newsResponse);
            var noticia = noticias1.FirstOrDefault(x => x.Permalink == id);
            noticias1 = noticias1.Where(x => x.Category.CategoryId == noticia.Category.CategoryId).ToList();

            noticia.Date = textInfo.ToTitleCase(noticia.Date); 

            var content = new IndexViewModel
            {
                Category = noticia
            };
            return View(content);
        }

        public IActionResult Search(string texto)
        {
            var apiUrl = "https://thecontent.mx/api";
            string mGuid = "676e30ab-286e-49c4-8723-114bd9238acd";
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");
                }
            }
            catch (Exception)
            {

            }

            var articles = JsonConvert.DeserializeObject<List<ArticleModel>>(newsResponse)
                .Where(x => x.Title.ToLower().Contains(texto.ToLower()))
                .OrderByDescending(x => x.NewsId)
                .ToList();



            return View(articles);
        }

        public IActionResult Contacto()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public ActionResult enviarCorreo(Mail model)
        {
            if (ModelState.IsValid)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Nombre:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Name}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Email:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Email}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Empresa:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Empresa}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Rol:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Enterprise}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Asunto:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Subject}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Teléfono:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Phone}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append($"Mensaje:");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append($"{model.Message}");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");

                var Mail = new IdentityMessage
                {
                    Body = sb.ToString(),
                    //Destination = "daniela@xipetechnology.com" + "oriol@xipetechnology.com" + "will@xipetechnology.com" + "patty@xipetechnology.com",
                    Destination = "carlos@xipetechnology.com",
                    Subject = "Contacto"
                };

                var resp = MailController.SendSimpleMessage(Mail);
                Response.StatusCode = (int)resp.StatusCode;
                if (Response.StatusCode == 200)
                {
                    return Ok();
                }
            }

            return BadRequest();


            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            //return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region set language method
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
        #endregion



    }
}
