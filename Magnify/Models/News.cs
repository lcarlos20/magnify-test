﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Magnify.Models
{
    public class News
    {
        public Int32 NewsId { set; get; }
        public Int32? UserId { set; get; }
        public String Title { set; get; }
        public String Description { set; get; }
        public String Image { set; get; }
        public String Body { set; get; }
        public DateTime CreationDate { set; get; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public String Date { set; get; }
        public Boolean IsDeleted { set; get; }
        public Int32 CategoryId { set; get; }
        //public Category Category { set; get; }
        public Int64? Rank { set; get; }
        public String Alt { set; get; }
        public String MetaDesc { set; get; }
        public String Keywords { set; get; }
        public String Permalink { set; get; }
        public String VideoEmbed { get; set; }
        public Boolean WithVideo { get; set; }
        //public List<Comment> Comments { get; set; }
        //public List<Visit> Visits { set; get; }
        //CLONING SYSTEM
        public Boolean IsClon { get; set; }
        public Int32 ClonedFrom { get; set; }
        public String ThankNote { get; set; }
        //LIKES SYSTEM
        //public List<Vote> Votes { get; set; }
    }
}
