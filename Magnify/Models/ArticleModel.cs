﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magnify.Models
{
    public class ArticleModel
    {

        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Permalink { get; set; }
        public string Body { get; set; }
        public string Date { get; set; }
        public string Alt { get; set; }
        public string EmbedVideo { get; set; }
        public bool WithVideo { get; set; }
        public CategoryModel Category { get; set; }
    }
}
