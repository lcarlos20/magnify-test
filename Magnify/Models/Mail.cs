﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Magnify.Models
{
    public class Mail
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Enterprise { get; set; }
        public string Empresa { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Phone { get; set; }

    }
}
