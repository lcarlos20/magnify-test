﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magnify.Models
{
    public class IndexViewModel
    {
        public List<News> News { get; set; }
        public ArticleModel Category { get; set; }
        public int PaginaActual { get; set; }
        public int PaginasCount { get; set; }

    }
}
